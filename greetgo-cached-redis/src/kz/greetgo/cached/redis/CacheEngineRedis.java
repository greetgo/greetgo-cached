package kz.greetgo.cached.redis;

import kz.greetgo.cached.core.main.CacheEngine;
import kz.greetgo.cached.core.main.CacheParamDefinition;
import kz.greetgo.cached.core.main.CacheParamDefinitionLong;
import kz.greetgo.cached.core.main.CoreCache;
import kz.greetgo.cached.core.main.CoreCacheEmpty;
import kz.greetgo.cached.core.main.MethodAnnotationData;
import kz.greetgo.cached.core.serialize.Serializer;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static kz.greetgo.cached.core.util.ReadUtil.readLong;

public class CacheEngineRedis implements CacheEngine {

  static final String LIFE_TIME_MILLIS = "lifeTimeMillis";

  static final String MAXIMUM_SIZE = "maximumSize";

  private final JedisPool  pool;
  private final Serializer serializer;

  public CacheEngineRedis(JedisPool pool, Serializer serializer) {
    this.pool       = pool;
    this.serializer = serializer;
  }

  @Override
  public List<CacheParamDefinition> paramList(MethodAnnotationData methodAnnotationData) {
    return List.of(
      CacheParamDefinitionLong.of(MAXIMUM_SIZE, "Максимальный размер элементов в кеше",
                                  methodAnnotationData.maximumSizeOr(1000))
      ,
      CacheParamDefinitionLong.of(LIFE_TIME_MILLIS, "Время жизни одного элемента в кеше в миллисекундах",
                                  methodAnnotationData.lifeTimeMillisOr(1000))
    );
  }

  @Override
  public <In, Out> CoreCache<In, Out> createCoreCache(Map<String, Object> cacheParams, String cacheId) {
    requireNonNull(cacheId, "dIa7FvxZQk :: cacheId");
    if (cacheId.indexOf('-') >= 0) {
      throw new RuntimeException("TA1RLLNmvB :: Illegal cache id. It cannot contains minus (-): " + cacheId);
    }

    long maximumSize = readLong(MAXIMUM_SIZE, cacheParams);

    if (maximumSize == 0) {
      return new CoreCacheEmpty<>(cacheParams);
    }

    long lifeTimeMillis = readLong(LIFE_TIME_MILLIS, cacheParams);

    return new CoreCache<In, Out>() {

      String toKey(In in) {
        return cacheId + '-' + serializer.serialize(in);
      }

      @Override
      public Out get(In in, Supplier<Out> direct) {

        String key = toKey(in);

        try (Jedis jedis = pool.getResource()) {

          {
            String cachedValue = jedis.get(key);
            if (cachedValue != null) {
              return serializer.deserialize(cachedValue);
            }
          }

          Out out = direct.get();

          String outStr = serializer.serialize(out);

          long seconds = lifeTimeMillis / 1000;

          if (seconds > 0) {
            jedis.setex(key, seconds, outStr);
          } else {
            jedis.set(key, outStr);
          }

          return out;
        }

      }

      @Override
      public Map<String, Object> params() {
        return cacheParams;
      }

      @Override
      public void invalidateAll() {

        try (Jedis jedis = pool.getResource()) {

          //noinspection SpellCheckingInspection
          jedis.eval("for _,k in ipairs(redis.call('keys', ARGV[1]..'*')) do  redis.call('del', k); end", 0, cacheId);

        }

      }

      @Override
      public void invalidateOn(In in) {

        String key = toKey(in);

        try (Jedis jedis = pool.getResource()) {
          jedis.del(key);
        }

      }

      @Override
      public void close() {
        // Nothing to do
      }
    };
  }

  public void closePool() {
    pool.close();
  }
}
