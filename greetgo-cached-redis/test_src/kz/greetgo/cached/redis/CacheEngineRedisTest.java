package kz.greetgo.cached.redis;

import kz.greetgo.cached.core.serialize.Serializer;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import redis.clients.jedis.JedisPool;

import java.util.Map;
import java.util.Random;

import static kz.greetgo.cached.redis.CacheEngineRedis.LIFE_TIME_MILLIS;
import static kz.greetgo.cached.redis.CacheEngineRedis.MAXIMUM_SIZE;
import static org.assertj.core.api.Assertions.assertThat;

public class CacheEngineRedisTest {

  private static final String     NULL_STR   = "<<NULL>>";
  private static final Serializer serializer = new Serializer() {
    @Override
    public String serialize(Object object) {
      return object == null ? NULL_STR : object.toString();
    }

    @Override
    public <T> T deserialize(String str) {
      if (NULL_STR.equals(str)) {
        return null;
      }
      //noinspection unchecked
      return (T) str;
    }
  };

  private JedisPool        pool;
  private CacheEngineRedis engine;

  @BeforeMethod
  public void createJedisPool() {
    pool   = new JedisPool("localhost", 17020);
    engine = new CacheEngineRedis(pool, serializer);
  }

  @AfterMethod
  public void closeJedisPool() {
    if (pool != null) {
      pool.close();
      pool = null;
    }
  }

  @Test
  public void checkCacheWorking() {

    var coreCache = engine.createCoreCache(Map.of(LIFE_TIME_MILLIS, 5000L, MAXIMUM_SIZE, 10L), "test");

    var asd1 = coreCache.get("asd", () -> "asd before");
    var asd2 = coreCache.get("asd", () -> "asd after");

    assertThat(asd1).isEqualTo("asd before");
    assertThat(asd2).isEqualTo("asd before");

  }

  @Test
  public void invalidateAll() {

    Random rnd    = new Random();
    int    rndInt = rnd.nextInt();
    if (rndInt < 0) {
      rndInt = -rndInt;
    }

    var coreCache1 = engine.createCoreCache(Map.of(LIFE_TIME_MILLIS, 150000L, MAXIMUM_SIZE, 10L), rndInt + "_test1");
    var coreCache2 = engine.createCoreCache(Map.of(LIFE_TIME_MILLIS, 150000L, MAXIMUM_SIZE, 10L), rndInt + "_test2");

    {
      var asd11 = coreCache1.get("asd1", () -> "asd1 value1");
      var asd12 = coreCache1.get("asd2", () -> "asd2 value1");
      var asd21 = coreCache2.get("asd1", () -> "asd1 value2");
      var asd22 = coreCache2.get("asd2", () -> "asd2 value2");

      assertThat(asd11).isEqualTo("asd1 value1");
      assertThat(asd12).isEqualTo("asd2 value1");
      assertThat(asd21).isEqualTo("asd1 value2");
      assertThat(asd22).isEqualTo("asd2 value2");
    }

    //
    //
    coreCache1.invalidateAll();
    //
    //

    {
      var asd11 = coreCache1.get("asd1", () -> "asd1 value1 x");
      var asd12 = coreCache1.get("asd2", () -> "asd2 value1 x");
      var asd21 = coreCache2.get("asd1", () -> "asd1 value2 x");
      var asd22 = coreCache2.get("asd2", () -> "asd2 value2 x");

      assertThat(asd11).isEqualTo("asd1 value1 x");
      assertThat(asd12).isEqualTo("asd2 value1 x");
      assertThat(asd21).isEqualTo("asd1 value2");
      assertThat(asd22).isEqualTo("asd2 value2");
    }

  }
}
