package kz.greetgo.cached.proxy.byte_buddy;

import kz.greetgo.cached.core.util.proxy.MethodCallHandler;
import kz.greetgo.cached.core.util.proxy.MethodProxyInvoker;
import kz.greetgo.cached.core.util.proxy.ProxyGenerator;
import lombok.NonNull;
import lombok.SneakyThrows;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.implementation.InvocationHandlerAdapter;
import net.bytebuddy.matcher.ElementMatchers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ProxyGeneratorByteBuddy implements ProxyGenerator {

  @Override
  @SneakyThrows
  @SuppressWarnings("Convert2Lambda")
  public Object createProxy(@NonNull Class<?> proxyInstanceClass,
                            @NonNull Object original,
                            @NonNull MethodCallHandler methodCallHandler) {

    try (DynamicType.Unloaded<?> make = new ByteBuddy()
      .subclass(proxyInstanceClass)
      .method(ElementMatchers.isDeclaredBy(proxyInstanceClass))
      .intercept(InvocationHandlerAdapter.of(new InvocationHandler() {
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
          return methodCallHandler.handle(proxy, method, args, new MethodProxyInvoker() {
            @Override
            public Object invokeSuper(Object proxyObject, Object[] args) throws Throwable {
              return method.invoke(original, args);
            }
          });
        }
      }))
      .make()) {
      return make.load(proxyInstanceClass.getClassLoader())
                 .getLoaded()
                 .getConstructor()
                 .newInstance();
    }

  }
}
