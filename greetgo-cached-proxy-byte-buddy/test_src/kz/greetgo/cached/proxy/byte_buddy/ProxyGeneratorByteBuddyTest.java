package kz.greetgo.cached.proxy.byte_buddy;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ProxyGeneratorByteBuddyTest {

  public static class TestClass {
    final List<String> lines = new ArrayList<>();

    public String method1(String arg) {
      lines.add("M9NukTL1so :: Calling method1(" + arg + ")");
      return arg + "-RET";
    }
  }

  @Test
  public void testProxyClass() {

    final ProxyGeneratorByteBuddy proxyGenerator = new ProxyGeneratorByteBuddy();

    TestClass original = new TestClass();

    final TestClass proxy = (TestClass) proxyGenerator.createProxy(
      TestClass.class, original, (proxyObject, method, args, methodProxyInvoker) -> {
        original.lines.add("0Vt1iOrO0S :: Proxy method " + method.getName());
        return method.invoke(original, args);
      });

    final String result = proxy.method1("test");

    assertThat(result).isEqualTo("test-RET");

    System.out.println(original.lines);

    assertThat(original.lines.get(0)).isEqualTo("0Vt1iOrO0S :: Proxy method method1");
    assertThat(original.lines.get(1)).isEqualTo("M9NukTL1so :: Calling method1(test)");
  }
}
