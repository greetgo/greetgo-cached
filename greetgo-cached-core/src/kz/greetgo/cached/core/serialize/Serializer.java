package kz.greetgo.cached.core.serialize;

public interface Serializer {

  String serialize(Object object);

  <T> T deserialize(String str);

}
