package kz.greetgo.cached.core.util.proxy;

import lombok.NonNull;

public interface ProxyGenerator {

  Object createProxy(@NonNull Class<?> proxyInstanceClass,
                     @NonNull Object original, @NonNull MethodCallHandler methodCallHandler);

}
