package kz.greetgo.cached.core.main;

/**
 * Обработчик для дополнительной конфигурации движка кеширования
 *
 * @param <CE> класс движка кеширования
 */
public interface CacheEngineConfigHandler<CE extends CacheEngine> {

  /**
   * Настраивает движок кеширования
   *
   * @param cacheEngine     ссылка на движок
   * @param cacheEngineName имя движка. Null - значит движок-по-умолчанию
   */
  void configure(CE cacheEngine, String cacheEngineName);

}
