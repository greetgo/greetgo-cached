package kz.greetgo.cached.probe_app;

import kz.greetgo.cached.core.Cached;
import kz.greetgo.cached.core.annotations.CacheGroup;
import kz.greetgo.cached.core.annotations.CacheLifeTimeMillis;
import kz.greetgo.cached.core.annotations.CacheMaximumSize;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ProbeCacheController {

  private final AtomicInteger helloIntStatus = new AtomicInteger(1);

  @CacheLifeTimeMillis(7000)
  @CacheMaximumSize(10000)
  @CacheGroup("BO")
  public Cached<String> helloInt(int value) {
    return () -> {
      SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
      return Optional.of(sdf.format(new Date()) + " Value " + value + " Status " + helloIntStatus.getAndIncrement());
    };
  }

  private final ConcurrentHashMap<String, AtomicInteger> manyArgsStatus = new ConcurrentHashMap<>();

  @SuppressWarnings("unused")
  @CacheLifeTimeMillis(7000)
  @CacheMaximumSize(10000)
  @CacheGroup("BO")
  public Cached<String> manyArgs(int value, String str, Date date) {
    return () -> {

      final int status = manyArgsStatus.computeIfAbsent(str, s -> new AtomicInteger(1)).getAndIncrement();

      String S = "" + status;
      while (S.length() < 3) {
        S = "0" + S;
      }

//      SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
//      SimpleDateFormat ddd = new SimpleDateFormat("yyyy-MM-dd");
//      return Optional.of(sdf.format(new Date()) + " Value (" + value + ", " + str + ", " + ddd.format(date)
//                           + "); Status " + status);

      return Optional.of("{ST:" + S + "/" + str + "}");

    };
  }

  public void resetStatuses() {
    manyArgsStatus.clear();
    helloIntStatus.set(1);
  }
}
