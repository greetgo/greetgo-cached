package kz.greetgo.cached.probe_app;

import kz.greetgo.cached.caffeine.CacheEngineCaffeine;
import kz.greetgo.cached.core.Cached;
import kz.greetgo.cached.core.file_storage.ParamsFileStorageFs;
import kz.greetgo.cached.core.main.CacheManager;
import kz.greetgo.util.fui.FUI;
import kz.greetgo.util.fui.IntAccessor;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProbeCacheApplication {
  public static void main(String[] args) throws Exception {
    new ProbeCacheApplication().execute();
  }

  private void execute() throws Exception {

    Path appDir = Path.of("build/probe_cache_application");

    final FUI                 fui                 = new FUI(appDir);
    final AtomicBoolean       fuiWorking          = new AtomicBoolean(true);
    final ParamsFileStorageFs paramsFileStorage   = new ParamsFileStorageFs(appDir.resolve("000-config"));
    final CacheEngineCaffeine cacheEngineCaffeine = new CacheEngineCaffeine();

    final CacheManager cacheManager = CacheManager.builder()
                                                  .useDefaultCacheEngine(cacheEngineCaffeine)
                                                  .paramsFileStorage(paramsFileStorage)
                                                  .proxyGenerator_useCglib()
                                                  .configFileExtension(".tst-conf")
                                                  .configErrorsFileExtension(".tst-conf-errors")
                                                  .accessParamsDelayMillis(100)
                                                  .currentTimeMillis(System::currentTimeMillis)
                                                  .build();

    final ProbeCacheController probeCacheController = cacheManager.cacheIt(new ProbeCacheController());

    fui.button("001-init-configs", cacheManager::initConfigs);

    final String      callSeveral      = "call-Several";
    final IntAccessor timeoutBetweenMs = fui.entryInt(callSeveral + "/timeout-between-ms", 1000);
    final IntAccessor callCount        = fui.entryInt(callSeveral + "/call-count", 3);
    final IntAccessor A                = fui.entryInt(callSeveral + "/A", 13);

    fui.button(callSeveral + "/100-reset-status", probeCacheController::resetStatuses);
    fui.button(callSeveral + "/110-invalidate-all", () -> {
      probeCacheController.helloInt(0).invalidateAll();
      probeCacheController.manyArgs(0, "", new Date()).invalidateAll();
    });

    final String commandDir  = callSeveral + "/command";
    final Path   commandFile = appDir.resolve(commandDir).resolve("command.txt");
    if (!Files.exists(commandFile)) {
      String commandContent = "" +
        "\n#invalidateAll" +
        "\ninvalidate    str-1" +
        "\n#invalidate   str-1   str-2   str-3   str-4   str-5" +
        "\n#invalidate str-101 str-102 str-103 str-104 str-105" +
        "\n#invalidate str-701 str-702 str-703 str-704 str-705" +
        "\n";
      commandFile.toFile().getParentFile().mkdirs();
      Files.writeString(commandFile, commandContent, StandardCharsets.UTF_8);
    }


    fui.button(commandDir + "/run", () -> executeCommands(commandFile, probeCacheController, A));

    for (int i = 1; i <= 7; i++) {
      final int           I       = i;
      final AtomicBoolean working = new AtomicBoolean(false);
      fui.button(callSeveral + "/01-manyArgs-" + I, () -> {
        if (working.get()) {
          working.set(false);
          return;
        }
        working.set(true);

        new Thread(() -> {

          for (int j = 0; working.get() && fuiWorking.get(); j++) {

            final int timeoutMs = timeoutBetweenMs.get();

            if (j > 0 && timeoutMs > 0) {
              try {
                Thread.sleep(timeoutMs);
              } catch (InterruptedException e) {
                return;
              }
            }

            try {

              final String     str1    = "str-" + I;
              final String     str2    = "str-" + (I + 100);
              final String     str3    = "str-" + (I + 700);
              SimpleDateFormat ddd     = new SimpleDateFormat("yyyy-MM-dd");
              final Date       date    = ddd.parse("2003-01-21");
              final String     result1 = probeCacheController.manyArgs(A.get(), str1, date).orElse(null);
              final String     result2 = probeCacheController.manyArgs(A.get(), str2, date).orElse(null);
              final String     result3 = probeCacheController.manyArgs(A.get(), str3, date).orElse(null);
              final String     sss     = String.join(", ", str1, str2, str3);
              SimpleDateFormat sdf     = new SimpleDateFormat("HH:mm:ss.SSS");
              System.out.println("hzMt4JrXh8 :: " + sdf.format(new Date())
                                   + " (" + A.get() + ", [" + sss + "], " + ddd.format(date) + ")"
                                   + "  ->  [" + result1 + ", " + result2 + ", " + result3 + "]");

            } catch (ParseException e) {
              throw new RuntimeException(e);
            }
          }

          System.out.println("LaoUSk9Oxx :: Finished calling manyArgs in thread " + Thread.currentThread().getName());

        }).start();
      });

    }

    for (int i = 100; i <= 103; i++) {
      int I = i;

      fui.button("hello/02-helloInt-" + I, () -> new Thread(() -> {

        final int callCnt = callCount.get();

        for (int j = 0; j < callCnt; j++) {

          final int timeoutMs = timeoutBetweenMs.get();

          if (j > 0 && timeoutMs > 0) {
            try {
              Thread.sleep(timeoutMs);
            } catch (InterruptedException e) {
              return;
            }
          }

          {
            String           result = probeCacheController.helloInt(I).opt().orElse(null);
            SimpleDateFormat sdf    = new SimpleDateFormat("HH:mm:ss.SSS");
            System.out.println("hzMt4JrXh8 :: " + sdf.format(new Date()) + " helloInt(" + I + ") returns " + result);
          }
        }

      }).start());
    }

    System.out.println("4wDrL5JB1I :: Application STARTED");

    fui.go();
    fuiWorking.set(false);

    System.out.println("cl71oIU07A :: Application FINISHED");
  }

  private void executeCommands(Path commandFile, ProbeCacheController probeCacheController, IntAccessor A)
    throws Exception {

    for (final String line : Files.readString(commandFile).split("\n")) {
      String trimmedLine = line.trim();
      if (trimmedLine.startsWith("#") || trimmedLine.isEmpty()) {
        continue;
      }

      final int idx = trimmedLine.indexOf(' ');
      String    cmd, args;
      if (idx < 0) {
        cmd  = trimmedLine;
        args = "";
      } else {
        cmd  = trimmedLine.substring(0, idx);
        args = trimmedLine.substring(idx + 1).trim();
      }

      if ("invalidate".equals(cmd)) {

        SimpleDateFormat ddd  = new SimpleDateFormat("yyyy-MM-dd");
        Date             date = ddd.parse("2003-01-21");

        for (final String str : args.split("\\s+")) {
          final Cached<String> cached = probeCacheController.manyArgs(A.get(), str, date);
          cached.invalidateOne();
          final String newValue = cached.orElseThrow();
          System.out.println("5H7J0Qy6N0 :: Invalidate (" + A.get() + ", " + str + ", " + ddd.format(date) + ")" +
                               " -> " + newValue);
        }


        continue;
      }

      if ("invalidateAll".equals(cmd)) {
        SimpleDateFormat ddd  = new SimpleDateFormat("yyyy-MM-dd");
        Date             date = ddd.parse("2003-01-21");
        probeCacheController.manyArgs(A.get(), "", date).invalidateAll();
        System.out.println("EXSmSi1eBj :: Executed invalidate all");

        continue;
      }

      throw new RuntimeException("0Q43zkrURf :: Unknown command: " + cmd);

    }

  }
}
